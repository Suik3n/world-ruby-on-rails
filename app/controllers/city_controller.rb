class CityController < ApplicationController
  def index
  end

  def details
    country_id = params['country_id']
    country = Country.find(country_id)
    @cities = country.cities
  end
end
