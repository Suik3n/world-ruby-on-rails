class LanguageController < ApplicationController
  def index
  end

  def details
    @country_id = params['country_id']
    @country = Country.find(@country_id)
    @languages = @country.languages
  end

  def cities
    language_id = params['language_id']
    language = Language.find(language_id)
    @countries = language.countries
    render :template => "country/index"
  end
end
