class CountryController < ApplicationController
  def index
    if params['sort'] && params['direction'] then
      @countries = Country.order(params['sort'] + " " + params['direction'])
    else
      @countries = Country.all
    end
    @text = "Tous les pays du monde"
    render :index
  end

  def europe
    @countries = Country.all(:conditions => {:Continent => "Europe"})
    @text = "Tous les pays d'europe"
    render :index
  end

  def tri_colonne
    @countries = Country.all(:conditions => {:Continent => "Europe"},:order => params['sort'])
    @text = "Tous les pays d'europe"
    if request.xml_http_request? then
      render :partial => "tri_colonne", :layout => false
    else
      render "index"
    end
  end
end
