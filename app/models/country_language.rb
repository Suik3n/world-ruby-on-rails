class CountryLanguage < ActiveRecord::Base
  attr_accessible :IsOfficial, :Percentage
  self.table_name = "CountryLanguage"
  belongs_to :country, :class_name => "Country", :foreign_key => "idCountry"
  belongs_to :language, :class_name => "Language", :foreign_key => "idLanguage"
end