class Language < ActiveRecord::Base
  attr_accessible :Name
  self.table_name = "Language"
  has_many :countries, :through => :countryLanguages, :foreign_key => "idLanguage"
  has_many :countryLanguages, :class_name => "CountryLanguage", :foreign_key => "idLanguage"
end
