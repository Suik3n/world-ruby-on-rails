class City < ActiveRecord::Base
  attr_accessible :Name, :CountryCode, :District, :Population
  self.table_name = "City"
  belongs_to :country, :class_name => "Country", :foreign_key => "idCountry"
end