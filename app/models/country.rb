class Country < ActiveRecord::Base
  self.table_name = "Country"
  attr_accessible :Code, :Name, :Continent, :Region, :SurfaceArea, :IndepYear, :Population, :LifeExpectancy, :GNP, :GNPOld,
    :LocalName, :GovernmentForm, :HeadOfState, :Capital, :Code2
  has_many :cities, :class_name => "City", :foreign_key => "idCountry"
  has_many :languages, :through => :countryLanguages
  has_one :capital, :class_name => "City", :foreign_key => "idCountry"
  has_many :countryLanguages, :class_name => "CountryLanguage", :foreign_key => "idCountry"
end
