require 'test_helper'

class LanguageControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get details" do
    get :details
    assert_response :success
  end

  test "should get cities" do
    get :cities
    assert_response :success
  end

end
